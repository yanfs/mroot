/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.mapper.system


import wang.encoding.mroot.common.mapper.SuperMapper
import wang.encoding.mroot.model.entity.system.Role
import java.math.BigInteger


/**
 * 角色 Mapper 接口层
 *
 * @author ErYang
 */
interface RoleMapper : SuperMapper<Role> {

    /**
     * 根据用户 id 查询角色
     *
     * @param userId 用户id
     * @return Set<Role>
     */
    fun listByUserId(userId: BigInteger): Set<Role>?

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据用户 id 查询角色
     *
     * @param userId 用户id
     * @return Role
     */
    fun getByUserId(userId: BigInteger): Role?

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 用户 id 和 角色 id 批量新增 用户-角色 表
     *
     * @param map Map<String, Any>
     *
     * @return Int
     */
    fun saveBatchByUserIdAndRoleIdArray(map: Map<String, Any>): Int?

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 用户 id 和 角色 id 批量删除 用户-角色 表
     *
     * @param map Map<String, Any>
     *
     * @return Int
     */
    fun removeBatchByUserIdAndRoleIdArray(map: Map<String, Any>): Int?

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 用户id 删除 用户-角色 表
     *
     * @param userId BigInteger
     *
     * @return Int
     */
    fun removeByUserId(userId: BigInteger): Int?

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 用户id 删除 删除 用户-角色 表
     *
     * @param map Map<String, Any>
     *
     * @return Int
     */
    fun removeByUserIdArray(map: Map<String, Any>): Int?

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End RoleMapper interface

/* End of file RoleMapper.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/mapper/system/RoleMapper.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
