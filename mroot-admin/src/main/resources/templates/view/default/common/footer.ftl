<#-- 底部开始 -->
<footer class="m-grid__item m-footer ">
    <div class="m-container m-container--fluid m-container--full-height m-page__container">
        <div class="m-footer__wrapper">
            <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
                <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
								<span class="m-footer__copyright">
									2015 - ${.now?string("yyyy")} &copy;
									<a href="https://gitee.com/eryang/mroot" class="m-link"
                                       target="_blank">
                                    ${I18N("message.system.develop")}
                                    </a>
								</span>
                </div>
                <div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
                    <ul class="m-footer__nav m-nav m-nav--inline m--pull-right">
                        <li class="m-nav__item">
                            <a href="#" class="m-nav__link">
											<span class="m-nav__link-text">
                                            ${SERVER_PORT}
                                            </span>
                            </a>
                        </li>
                        <li class="m-nav__item">
                            <a href="#" class="m-nav__link">
											<span class="m-nav__link-text">
                                            ${PROFILE}
                                            </span>
                            </a>
                        </li>
                        <li class="m-nav__item">
                            <a href="javascript:" class="m-nav__link" data-toggle="m-tooltip"
                               title="${SESSION_ID}" data-placement="top">
                                <span class="m-nav__link-text">SESSION_ID</span>
                            </a>
                        </li>
                        <li class="m-nav__item">
                            <a class="m-nav__link" id="language_zh" data-value="zh"
                               data-url="${CONTEXT_PATH}/language/zh"
                               href="javascript:">
                                <span class="m-nav__link-text">${I18N("message.language.zh")}</span>
                            </a>
                        </li>
                        <li class="m-nav__item">
                            <a class="m-nav__link" id="language_en"
                               data-value="en"
                               data-url="${CONTEXT_PATH}/language/en"
                               href="javascript:">
                                <span class="m-nav__link-text">${I18N("message.language.en")}</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<#-- 底部结束 -->
