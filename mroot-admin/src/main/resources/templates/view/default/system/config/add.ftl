<@OVERRIDE name="MAIN_CONTENT">

    <#include "/default/common/pageAlert.ftl">

<#-- 内容开始 -->
<div class="m-portlet m-portlet--mobile">

    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    ${I18N("message.form.head.title")}
                </h3>
            </div>
        </div>
    </div>

<#-- 表单开始 -->
    <form class="m-form m-form--state m-form--fit m-form--label-align-right" id="config_form" action="${save}"
          method="post" autocomplete="off">
        <div class="m-portlet__body">

            <#include "/default/common/formAlert.ftl">

            <@formTokenAndRefererUrl></@formTokenAndRefererUrl>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.form.type")}&nbsp;*
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">

                    <div class="m-select2 m-select2--air">
                        <select id="typeSelect" class="form-control m-select2" name="type"
                                data-placeholder="${I18N("message.form.type.text.help")}">
                            <option></option>
                         <#list CONFIG_TYPE_MAP?keys as key>
                            <option <#if (config.type == key)>selected</#if> value="${key}">
                                ${CONFIG_TYPE_MAP[key]}
                            </option>
                         </#list>
                        </select>
                        <span class="m-form__help">${I18N("message.form.type.text.help")}</span>
                    </div>
                    <div class="m--space-10"></div>

                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.form.name")}&nbsp;*
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" class="form-control m-input" name="name"
                           placeholder="${I18N("message.form.name.text")}"
                           value="${config.name}"
                    >
                    <span class="m-form__help">${I18N("message.form.name.text.help")}</span>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.form.title")}&nbsp;*
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" class="form-control m-input" name="title"
                           placeholder="${I18N("message.form.title.text")}"
                           value="${config.title}"
                    >
                    <span class="m-form__help">${I18N("message.form.title.text.help")}</span>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.system.config.form.value")}&nbsp;*
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                   <textarea class="form-control" rows="3" name="value" id="value"
                             placeholder="${I18N("message.system.config.form.value.text")}"
                   >${config.value}</textarea>
                    <span class="m-form__help">${I18N("message.system.config.form.value.text.help")}</span>
                </div>
            </div>

            <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.form.sort")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" class="form-control m-input" name="sort" id="sort"
                           placeholder="${I18N("message.form.sort.text")}"
                           value="${role.sort}"
                    >
                    <span class="m-form__help">${I18N("message.form.sort.text.help")}</span>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.form.remark")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                   <textarea class="form-control" rows="3" name="remark" id="remark"
                             placeholder="${I18N("message.form.remark.text")}"
                   >${role.remark}</textarea>
                    <span class="m-form__help">${I18N("message.form.remark.text.help")}</span>
                </div>
            </div>


        </div>

    <#-- 提交按钮 -->
               <@formOperate></@formOperate>

    </form>
<#-- 表单结束 -->

</div>
<#-- 内容结束 -->

</@OVERRIDE>
<@OVERRIDE name="PAGE_MESSAGE">
    <#include "/default/system/config/configJs.ftl">}
</@OVERRIDE>
<#include "/default/scriptPlugin/form.ftl">
<@OVERRIDE name="CUSTOM_SCRIPT">
<script>
    jQuery(document).ready(function () {
        // 顶部导航高亮
        Tool.highlight_top_nav('${navIndex}');

        FormValidation.formValidationConfig();

        Tool.highlightSelect2('#typeSelect',<@i18nType language></@i18nType>,
                '${I18N("message.form.select.text")}');

        Tool.highlightTouchSpin('#sort',${maxSort});

        autosize($('#value'));

        autosize($('#remark'));
    });
</script>
</@OVERRIDE>

<@EXTENDS name="/default/common/base.ftl"/>
