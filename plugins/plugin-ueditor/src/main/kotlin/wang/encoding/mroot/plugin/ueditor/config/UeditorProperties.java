/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.plugin.ueditor.config;

import com.qiniu.common.Zone;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;

/**
 * 百度 ueditor 配置
 *
 * @author ErYang
 */
@PropertySource(value = "classpath:ueditor.properties")
@ConfigurationProperties(prefix = "ueditor")
public class UeditorProperties {

    private String config = "{}";

    private String accessKey;

    private String secretKey;

    /**
     * 七牛机房  华东：zone0 华北：zone1 华南：zone2 北美：zoneNa0
     */
    private String zone = "autoZone";

    private String bucket;

    private String baseUrl;

    private String uploadDirPrefix = "ueditor/file/";

    /**
     * 是否上传到本地
     */
    private Boolean uploadLocal;

    public String getConfig() {
        return config;
    }

    public void setConfig(String config) {
        this.config = config;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getUploadDirPrefix() {
        return uploadDirPrefix;
    }

    public void setUploadDirPrefix(String uploadDirPrefix) {
        this.uploadDirPrefix = uploadDirPrefix;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public Boolean getUploadLocal() {
        return uploadLocal;
    }

    public void setUploadLocal(Boolean uploadLocal) {
        this.uploadLocal = uploadLocal;
    }

    public Zone getZoneObj() {
        // 华东：zone0 华北：zone1 华南：zone2 北美：zoneNa0
        switch (zone) {
            case "zone0":
                return Zone.zone0();
            case "zone1":
                return Zone.zone1();
            case "zone2":
                return Zone.zone2();
            case "zoneNa0":
                return Zone.zoneNa0();
            default:
                return Zone.autoZone();
        }
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End UeditorProperties class

/* End of file UeditorProperties.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/plugin/ueditor/config/UeditorProperties.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
